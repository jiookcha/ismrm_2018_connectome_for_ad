%% volume
subjectlist=importdata('list_subject_hippo_vol');
HIPPO=[];
for s=1:length(subjectlist)
    subject=subjectlist(s)
f_lh=['hippo_vol_lh_' num2str(subject) '.txt'];
f_rh=['hippo_vol_rh_' num2str(subject) '.txt'];

lh=import_hippo_vol(f_lh)';rh=import_hippo_vol(f_rh)';
hippo=[lh,rh];
HIPPO=[HIPPO;hippo];
end


%% DTI
!ls hippo_FA_lh_*.txt | egrep -o '[0-9]{8}' > list_hippo_fa
subjectlist=importdata('list_hippo_fa');
HIPPO=[];
for s=1:length(subjectlist)
    subject=subjectlist(s)
f_lh=['hippo_FA_lh_' num2str(subject) '.txt'];
f_rh=['hippo_FA_rh_' num2str(subject) '.txt'];

lh=dlmread(f_lh)';rh=dlmread(f_rh)';
hippo=[lh,rh];
HIPPO=[HIPPO;hippo];
end
